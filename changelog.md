## Differences from original fork (`commit 0d701a9`):

Changes that are not stated here, can be found in the project commits.

### V1

- Introduced two methods:
  - _resizeRepeatStart
  - _resizeRepeatEnd
- Introduced two new event handlers:
  - zoomstart
  - zoomend
- New functionalities:
  - Markers now periodically refresh during a zoom animation.