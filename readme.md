# leaflet-markers-canvas

This is a fork of [francoisromain/leaflet-markers-canvas](https://gitlab.com/openzone-dev/leaflet-markers-canvas) project. Includes new features used in realtime applications.

## Description

A Leaflet plugin to render many markers in a canvas instead of the DOM.

This is a complete rewrite of [Leaflet.Canvas-Markers](https://github.com/eJuke/Leaflet.Canvas-Markers) by Eugene Voynov. Thank you for the inspiration.

## Features

- Image marker compatible
- Popup compatible
- Minimal code footprint

## Limitations

- No support for Div `Marker` objects

## Demo

Here is a [demo](https://francoisromain.github.io/leaflet-markers-canvas/examples/) of 10000 markers, displayed in one canvas.

## Usage

### Dependencies

[Leaflet](https://leafletjs.com/) and [RBush](https://github.com/mourner/rbush) must be available globally or installed as peer-dependencies.

### Install

- Install from npm: `npm i leaflet-markers-canvas`
- or download [leaflet-markers-canvas.js](https://github.com/francoisromain/leaflet-markers-canvas/blob/master/dist/leaflet-markers-canvas.js)

### Example

```js
var map = L.map("map").setView([59.9578, 30.2987], 10);
var tiles = L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
  attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>',
  preferCanvas: true,
}).addTo(map);

var markersCanvas = new L.MarkersCanvas();
markersCanvas.addTo(map);

var icon = L.icon({
  iconUrl: "marker.png",
  iconSize: [20, 32],
  iconAnchor: [10, 0],
});

var markers = [];

for (var i = 0; i < 10000; i++) {
  var marker = L.marker(
    [58.5578 + Math.random() * 1.8, 29.0087 + Math.random() * 3.6],
    { icon }
  )
    .bindPopup("I Am " + i)
    .on({
      mouseover(e) {
        this.openPopup();
      },
      mouseout(e) {
        this.closePopup();
      },
    });

  markers.push(marker);
}

markersCanvas.addMarkers(markers);
```

## Methods

### `addTo(map)`

Adds the canvas layer to the map. Should be called before adding any markers.

### `getBounds()`

Returns a LatLngBounds containing all the markers.

### `redraw()`

Redraws all the visible markers on the map again. Useful for updating many markers at once.

### `clear()`

Removes all markers.

### `addMarker(marker)`

Adds a marker to the canvas layer. Accepts `Marker` (or `L.marker`) objects. Keep in mind that there's no need to add the marker itself to the map, as the plugin will handle this.

### `addMarkers(markers)`

Adds an `Array` of `Marker` to the canvas layer. Accepts an iterable object (.forEach() capable). This is the preferred method of rendering many markers quickly.

### `removeMarker(marker)`

Removes a `Marker` object from the canvas layer. The implementing user is responsible for keeping a reference to the `Marker` objects shown on the canvas layer.

### `removeMarkers(markers)`

Removes all the `Markers` objects contained in the passed `Array`. This is the preferred method of removing many `Marker` objects quickly.

## To-do

- Complete documentation
